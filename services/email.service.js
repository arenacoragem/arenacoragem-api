const fs = require('fs');
const AWS = require('aws-sdk');
const Mustache = require('mustache');
var htmlToText = require('html-to-text');

AWS.config.update({ region: process.env.AWS_REGION });

const EmailService =  {
    resultHtml: function(user) {
        return Mustache.render(
            fs.readFileSync('./services/data/result.html', 'utf8'),
            {user: user});
    },

    resultText: function(user) {
        return htmlToText.fromString(this.resultHtml(user), {
            wordwrap: 130,
            preserveNewlines: true,
            ignoreImage: true
        }).trim().replace(/\n\n\n/g, '\n');
    },

    sendResultEmail: function(user) {
        const params = {
            Destination: {
                ToAddresses: [ user.email ]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: this.resultHtml(user)
                    },
                    Text: {
                        Charset: 'UTF-8',
                        Data: this.resultText(user)
                    }
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: 'Revista Coragem'
                }
            },
            Source: '"ThoughtWorks Brasil" <arenacoragem@thoughtworks.com>'
        };
        return new AWS.SES({apiVersion: '2010-12-01'})
            .sendEmail(params).promise();
    }
};

module.exports = EmailService;
