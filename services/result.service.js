
module.exports = class ResultService {
    constructor(person) {
        this.answers = person.answers;
    }

    calculateProfile() {
        const counts = {};

        this.answers.forEach((answer) => {
            if (!(answer.value in counts)) {
                counts[answer.value] = 0;
            }
            counts[answer.value] += 1;
        });

        const maxAnswer = Object.keys(counts).reduce((max, val) => {
            return (counts[val] > counts[max]) ? val : max;
        }, Object.keys(counts)[0]);

        return maxAnswer;
    }
};
