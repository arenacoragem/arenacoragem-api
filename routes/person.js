const express = require('express');
const winston = require('winston');
const Person = require('../models/person.model');
const Option = require('../models/option.model');
const Profile = require('../models/profile.model');
const ImageService = require('../services/image.service.js');
const EmailService = require('../services/email.service.js');
const ResultService = require('../services/result.service.js');

module.exports = (app) => {
    const router = express.Router();
    app.use('/person', router);

    this.index2Id = ['', ''];
    this.index2Words = [[], []];

    function addOptionToAnswers(req, option, res) {
        Person.findByIdAndUpdate(req.params.id, {
            $push: {
                answers: {
                    question: option.questionId.description,
                    option: option.description,
                    value: option.value,
                    book: option.book
                }
            }
        }, {new: true}).then((result) => {
            if (!result) {
                res.status(500).send({ success: false, message: 'Id não encontrado', data: result });
            }
            else {
                result.profile = new ResultService(result).calculateProfile();
                res.status(201).send({ success: true, message: 'Resposta adicionada com sucesso!', data: result });
            }
        }, (err) => {
            res.status(500).send({ success: false, message: '', data: err });
        });
    }

    router.get('/', (req, res) => {
        res.status(200).json({ message: 'person API OK' });
    });

    router.get('/:id', (req, res) => {
        Person.findById(req.params.id).then(
            (result) => {
                if (!result) {
                    res.status(500).send({ success: false, message: 'Id não encontrado', data: result });
                } else {
                    res.json(result);
                }
            }, (err) => {
                res.status(500).send({ success: false, message: 'Erro recuperar item', data: err });
            });
    });

    router.post('/', function (req, res) {
        var person = new Person(req.body);
        person.save().then(
            (result) => {
                res.status(201).send({ success: true, message: 'Obrigado!', data: result._doc });
            },
            (err) => {
                res.status(500).send({ success: false, message: 'Erro ao salvar os dados. Tente novamente!', data: err });
            });
    });

    router.post('/:shelfId', function(req, res) {
        const mId = Math.max(Math.min(parseInt(req.params.shelfId), 1), 0);

        var person = new Person(req.body);
        this.index2Id[mId] = person._id;
        this.index2Words[mId] = [];

        person.save().then(
            (result) => {
                res.status(201).send({ success: true, message: 'Obrigado!', data: result._doc });
            },
            (err) => {
                res.status(500).send({ success: false, message: 'Erro ao salvar os dados. Tente novamente!', data: err });
            });
    }.bind(this));

    router.get('/:id/results', function (req, res) {
        Person.findById(req.params.id, (err, person) => {
            if (err) {
                winston.error(err);
                res.status(500).send({ success: false, message: 'Erro ao recuperar dados do perfil. Tente novamente!', data: err });
                return;
            }
            try {
                Profile.findOne({ identifier: person.profile }, (err, profile) => {
                    res.json(profile);
                });
            } catch (error) {
                res.status(500).send({ success: false, message: 'Erro ao recuperar dados do perfil. Tente novamente!', data: error });
            }
        });
    });

    router.put('/:id/lights', function (req, res) {
        const mId = (this.index2Id[0] === req.params.id) ? 0 : 1;
        this.index2Words[mId] = [];
        Person.findById(req.params.id, (err, person) => {
            if (err) {
                winston.error(err);
                res.status(500).send({ success: false, message: 'Erro ao calcular luzes. Tente novamente!', data: err });
                return;
            }
            const books = {};
            person.answers.forEach((answer) => {
                books[answer.book] = '1';
            });
            books['14'] = '1';
            books['15'] = '1';
            this.index2Words[mId] = Object.keys(books).map((k) => parseInt(k));

            res.status(200).send({ success: true, message: 'Luzes atualizadas!', data: person });
        });
    }.bind(this));

    router.put('/:id', function (req, res) {
        const saveError = function(err) {
            winston.error(err);
            res.status(500).send({ success: false, message: 'Erro ao tentar atualizar pessoa.', data: err });
        };

        Person.findById(req.params.id, (err, person) => {
            if (err) {
                saveError(err);
                return;
            }

            person.name = req.body.name;
            person.email = req.body.email;
            person.company = req.body.company;
            person.role = req.body.role;

            try {
                person.profile = new ResultService(person).calculateProfile();
                person.save().then((result) => {
                    res.status(200).send({ success: true, message: 'Pessoa atualizada!', data: result });
                }).catch(saveError);

            } catch(error) {
                saveError(error);
                return;
            }
        });
    });

    router.delete('/:id', function (req, res) {
        Person.remove({ _id: req.params.id }).then(
            (result) => {
                res.status(204).send({ success: true, message: 'Pessoa removida!', data: result });
            },
            (err) => {
                res.status(500).send({ success: false, message: 'Erro ao tentar remover pessoa. Tente novamente!', data: err });
            });
    });

    router.put('/:id/answer/:optionId', function (req, res) {
        Option.findById(req.params.optionId)
            .populate('questionId')
            .then(
                (option) => {
                    addOptionToAnswers(req, option, res);
                },
                (err) => {
                    res.status(500).send({ success: false, message: 'Erro ao selecionar resposta', data: err });
                });
    });

    router.post('/:id/photo', (req, res) => {
        const sendError = function (err) {
            res.status(500).send({ success: false, message: 'Erro ao tentar subir imagem.', data: err });
        };

        const imageService = new ImageService(req.body);

        imageService.uploadImage((err, result) => {
            if (err) {
                sendError(err);
            } else {
                Person.findOneAndUpdate({ _id: req.params.id }, result, {new: true})
                    .then((result) => {
                        return Profile.findOne({ identifier: result.profile })
                            .select('-_id -__v')
                            .exec();
                    }).then((profile) => {
                        if(!profile) winston.error('no profile found');
                        result._id = req.params.id;
                        result = Object.assign({}, result, profile._doc);
                        return EmailService.sendResultEmail(result);
                    }).then(() => {
                        res.status(200).send({ success: true, message: 'Imagem atualizada!', data: result });
                    }).catch(sendError);
            }
        });
    });

    router.get('/photos/:since', (req, res) => {
        var searchQuery = {
            $and: [
                { 'datetime': { $gt: req.params.since } },
                { 'image_permission': true }
            ]
        };

        Person.find(searchQuery, { image_url: 1, profile: 1, datetime: 1 })
            .sort({ 'datetime': -1 })
            .then((result) => {
                res.json(result);
            }, (err) => {
                res.status(500).send({ success: false, message: 'Erro na busca', data: err });
            });
    });

    router.get('/latestWords/:shelfId', function(req, res) {
        const mId = req.params.shelfId;
        res.json(this.index2Words[mId]);
    }.bind(this));
};
