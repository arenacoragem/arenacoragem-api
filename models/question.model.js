const mongoose = require('mongoose');
const { Schema } = mongoose;
require('./option.model');

const QuestionSchemas = new mongoose.Schema({
    description: {
        type: String,
        required: true
    },
    order: {
        type: Number,
        required: true
    },
    options: {
        type: [{ type: Schema.Types.ObjectId, ref: 'option' }]
    }
});

const Question = mongoose.model('question', QuestionSchemas);

module.exports = Question;
