const mongoose = require('mongoose');
const crc = require('crc');

const generateId = () => {
    let id = crc.crc32(new Date().toISOString()).toString(24) + '12345678';
    return id.toUpperCase().slice(0, 6);
};

const PersonSchema = new mongoose.Schema({
    _id: {
        type: String,
        'default': generateId
    },
    name: {
        type: String
    },
    email: {
        type: String
    },
    company: {
        type: String
    },
    role: {
        type: String
    },
    profile: {
        type: String
    },
    image_url: {
        type: String
    },
    image_permission: {
        type: Boolean
    },
    datetime: {
        type: Date
    },
    answers:
        [{
            question: String,
            option: String,
            value: String,
            book: Number
        }]
});

const Person = mongoose.model('person', PersonSchema);

module.exports = Person;