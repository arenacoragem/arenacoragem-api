const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    identifier: {
        type: String,
        required: true
    },
    article_image_url: {
        type: String,
        required: true
    },
    article_url: {
        type: String,
        required: true
    },
    article_title: {
        type: String,
        required: true
    },
    article_intro: {
        type: String,
        required: true
    }
});

const Profile = mongoose.model('profile', ProfileSchema);

module.exports = Profile;