require('dotenv').load();
const winston = require('winston');
const EmailService = require('../services/email.service.js');

const user = {
    email: 'thiago.hersan@gmail.com',
    name: 'thiago',
    image_url: 'https://s3.amazonaws.com/tw-arena-coragem-2018/I494CA_JNL23RCP.jpg',
    article_image_url: 'https://s3.amazonaws.com/tw-coragem/INOVACAO_PRODUTOS.jpg',
    article_url: 'https://medium.com/coragem/tudo-pronto-e-funcionando-em-3-meses-c855dcb48061',
    article_title: 'Tudo pronto e funcionando em 3 meses',
    article_intro: 'Apple, Facebook, Spotify e AirBnB. O que essas empresas teriam em comum? Suas idealizadoras trabalharam de forma simples e efetiva, colocando produtos no mercado com rapidez e evoluindo com base na necessidade real das pessoas que utilizam seus produtos e serviços. Atitudes que minimizam seus gastos e fazem você existir no agora. Pois em tempos de inovação disruptiva  —  aquela que altera o seu negócio pela raiz  —  faz-se urgente saber lidar com a aceleração digital. Afinal, você não quer desperdiçar tempo, dinheiro e energia construindo um produto que não vai atender às expectativas de seu público final, certo?'
};

EmailService.sendResultEmail(user).then((data) => {
    winston.info(data.MessageId);
}).catch((err) => {
    winston.error(err.stack);
});
