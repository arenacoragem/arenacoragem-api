require('dotenv').load();
const mongoose = require('mongoose');
const winston = require('winston');
const Person = require('../models/person.model');

mongoose.connect(process.env.MONGODB_URI_PROD)
    .then(() => { 
        winston.info('Connected to database');
        findNull();
    }, (err) => {
        winston.error('erro ao abrir db: ' + err);
    });

var findNull = function() {
    var deleteForReal = (process.argv[2] === '-D');

    var searchQuery = {
        $or: [
            { 'email': { '$regex': /thersa/ } },
            //{ 'email': { '$regex': /tw/i } },
            //{ 'company': { '$regex': /tw/i } },
            { $and: [
                { 'email': { '$eq': null } },
                { 'name': { '$eq': null } }
            ]}
        ]
    };

    if(deleteForReal) {
        winston.info('Deleting for real');
        Person.remove(searchQuery).then(() => {
            winston.info('Deleted');
            mongoose.connection.close();
        }, (err) => {
            winston.error('Erro na busca' + err);
        });
    } else {
        Person.find(searchQuery).then((result) => {
            result.forEach((person) => {
                winston.info(`${person.name} ${person.email} ${person.company}`);
            });
            winston.info(result.length);
            winston.info('This was a dry run. To REALLY delete, re-run script with -D option');
            mongoose.connection.close();
        }, (err) => {
            winston.error('Erro na busca' + err);
        });
    }
};
