require('dotenv').load();

const path = require('path');
const Question = require('../models/question.model');
const Option = require('../models/option.model');
const Profile = require('../models/profile.model');
const seeder = require('mongoose-seed');

seeder.connect(process.env.MONGODB_URI, () => {
    seeder.loadModels([
        path.resolve(__dirname, '../models/question.model.js'),
        path.resolve(__dirname, '../models/option.model.js'),
        path.resolve(__dirname, '../models/profile.model.js'),
    ]);

    seeder.clearModels(['question', 'option', 'profile'], function () {

        seeder.populateModels(data, function () {
            seeder.disconnect();
        });
    });
});


let data = [
    {
        'model': 'question',
        'documents': []
    },
    {
        'model': 'option',
        'documents': []
    },
    {
        'model': 'profile',
        'documents': []
    }
];

function setQuestion(question, options) {
    const questionModel = new Question(question);

    for (const option of options) {
        const optionModel = new Option(option);
        optionModel.questionId = questionModel._id;

        questionModel.options.push(optionModel);
        data[1].documents.push(optionModel);
    }

    data[0].documents.push(questionModel);
}

function setProfile(profileObj) {
    const profile = new Profile(profileObj);
    data[2].documents.push(profile);
}

setProfile({
    title: 'Inovação de Produtos',
    identifier: 'INOVACAO_PRODUTOS',
    article_image_url: 'https://s3.amazonaws.com/tw-coragem/INOVACAO_PRODUTOS.jpg',
    article_url: 'https://medium.com/coragem/tudo-pronto-e-funcionando-em-3-meses-c855dcb48061',
    article_title: 'Tudo pronto e funcionando em 3 meses',
    article_intro: 'Apple, Facebook, Spotify e AirBnB. O que essas empresas teriam em comum? Suas idealizadoras trabalharam de forma simples e efetiva, colocando produtos no mercado com rapidez e evoluindo com base na necessidade real das pessoas que utilizam seus produtos e serviços. Atitudes que minimizam seus gastos e fazem você existir no agora. Pois em tempos de inovação disruptiva  —  aquela que altera o seu negócio pela raiz  —  faz-se urgente saber lidar com a aceleração digital. Afinal, você não quer desperdiçar tempo, dinheiro e energia construindo um produto que não vai atender às expectativas de seu público final, certo?'
});

setProfile({
    title: 'Liderança Adaptativa',
    identifier: 'LIDERANCA_ADAPTATIVA',
    article_image_url: 'https://s3.amazonaws.com/tw-coragem/LIDERANCA_ADAPTATIVA.jpg',
    article_url: 'https://medium.com/coragem/lideran%C3%A7as-corajosas-462368ab1c65',
    article_title: 'Lideranças corajosas',
    article_intro: 'Disrupção. A palavra que representa o que está acontecendo agora no mundo. Empresarial, tecnológico, pessoal. O termo apareceu pela primeira vez em 1995, quando Clayton Christensen, professor de Harvard, publicou um artigo apresentando o conceito de Disrupção Tecnológica, inspirado no conceito de “destruição criativa”, cunhado em 1939 pelo economista austríaco Joseph Schumpeter para explicar os ciclos de negócios do capitalismo. Segundo ele, cada nova revolução (tecnológica ou industrial) destrói a anterior e toma seu mercado.'
});

setProfile({
    title: 'Transformação Digital',
    identifier: 'TRANSFORMACAO_DIGITAL',
    article_image_url: 'https://s3.amazonaws.com/tw-coragem/TRANSFORMACAO_DIGITAL.jpg',
    article_url: 'https://medium.com/coragem/ningu%C3%A9m-%C3%A9-mais-o-que-foi-ou-ser%C3%A1-108624918ad1',
    article_title: 'Ninguém é mais o que foi ou será',
    article_intro: 'Vivemos em um mundo em transformação. Embora sempre possamos afirmar isso — afinal, o mundo está em constante mudança a todo momento — , há certas épocas em que um conjunto de elementos converge e quebra paradigmas, valores e comportamentos do passado. É o caso das revoluções industriais. Estamos vivendo hoje a Quarta Revolução Industrial, e este é um desses momentos históricos transformadores.'
});

setQuestion(
    {
        'description': 'Com quais dessas tendências tecnológicas você mais se identifica?',
        'order': 0,
        'options': []
    },
    [{
        'description': 'Produtos e serviços digitais',
        'value': 'INOVACAO_PRODUTOS',
        'book': 7
    },
    {
        'description': 'Realidade virtual e aumentada',
        'value': 'INOVACAO_PRODUTOS',
        'book': 4
    },
    {
        'description': 'Segurança, privacidade e transparência',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 12
    },
    {
        'description': 'Inteligência artificial e aprendizagem de máquinas',
        'value': 'INOVACAO_PRODUTOS',
        'book': 8
    },
    {
        'description': 'Robôs e veículos auto-dirigíveis',
        'value': 'TRANSFORMACAO_DIGITAL',
        'book': 8
    }]
);

setQuestion(
    {
        'description': 'Qual o maior desafio para o seu negócio?',
        'order': 1,
        'options': []
    },
    [{
        'description': 'Crescimento',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 5
    },
    {
        'description': 'Transformação Digital',
        'value': 'TRANSFORMACAO_DIGITAL',
        'book': 6
    },
    {
        'description': 'Inovação',
        'value': 'INOVACAO_PRODUTOS',
        'book': 6
    },
    {
        'description': 'Aprendizado contínuo',
        'value': 'TRANSFORMACAO_DIGITAL',
        'book': 9
    },
    {
        'description': 'Fazer parcerias produtivas',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 5
    }]
);

setQuestion(
    {
        'description': 'Qual dessas características você valoriza mais em uma parceria de negócios?',
        'order': 2,
        'options': []
    },
    [{
        'description': 'Conhecimento técnico',
        'value': 'INOVACAO_PRODUTOS',
        'book': 12
    },
    {
        'description': 'Adaptabilidade',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 6
    },
    {
        'description': 'Confiança',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 10
    },
    {
        'description': 'Experiência',
        'value': 'INOVACAO_PRODUTOS',
        'book': 4
    },
    {
        'description': 'Novas metodologias',
        'value': 'TRANSFORMACAO_DIGITAL',
        'book': 10
    }]
);

setQuestion(
    {
        'description': 'Qual é a maior motivação para mudanças no seu negócio?',
        'order': 3,
        'options': []
    },
    [{
        'description': 'Crescimento de empresas concorrentes',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 11
    },
    {
        'description': 'Busca por novos clientes',
        'value': 'INOVACAO_PRODUTOS',
        'book': 7
    },
    {
        'description': 'Comportamento/Opinião de atuais clientes',
        'value': 'TRANSFORMACAO_DIGITAL',
        'book': 8
    },
    {
        'description': 'Influência de empresas parceiras',
        'value': 'LIDERANCA_ADAPTATIVA',
        'book': 5
    },
    {
        'description': 'Mudanças culturais',
        'value': 'INOVACAO_PRODUTOS',
        'book': 9
    }]
);
