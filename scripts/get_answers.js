require('dotenv').load();
const mongoose = require('mongoose');
const winston = require('winston');
const Person = require('../models/person.model');
const fs = require('fs');

mongoose.connect(process.env.MONGODB_URI_PROD)
    .then(() => { 
        winston.info('Connected to database');
        gererateCsv();
    }, (err) => {
        winston.error('erro ao abrir db: ' + err);
    });

var gererateCsv = function() {
    let questionAnswers = {};
    Person.find({}).then((result) => {
        result.forEach((person) => {
            person.answers.forEach((answer) => {
                if (!(answer.question in questionAnswers)) {
                    questionAnswers[answer.question] = [];
                }
                questionAnswers[answer.question].push(answer.option);
            });
        });

        let answersCsv = '';
        Object.keys(questionAnswers).forEach((q) => {
            answersCsv += q + ',\n';
            questionAnswers[q].forEach((a) => {
                answersCsv += a + ',\n';
            });
            answersCsv += '\n';
        });

        /**/
        fs.writeFile('hsm2018.answers.csv', answersCsv, function(err) {
            if(err) {
                winston.error(err);
                return;
            }
            winston.info(result.length);
        });
        /**/

        mongoose.connection.close();
    }, (err) => {
        winston.error('Erro na busca' + err);
    });
};
