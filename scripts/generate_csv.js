require('dotenv').load();
const mongoose = require('mongoose');
const winston = require('winston');
const Person = require('../models/person.model');
const fs = require('fs');

mongoose.connect(process.env.MONGODB_URI_PROD)
    .then(() => { 
        winston.info('Connected to database');
        gererateCsv();
    }, (err) => {
        winston.error('erro ao abrir db: ' + err);
    });

var gererateCsv = function() {
    Person.find({})
        .then((result) => {
            let profileCsv = 'id,name,email,company,role,profile\n';
            result.forEach((person) => {
                let personString = `${person._id},` +
                    `${person.name},` +
                    `${person.email},` +
                    `${person.company},` +
                    `${person.role},` +
                    `${person.profile}`;
                winston.info(personString);
                profileCsv += personString + '\n';
            });

            fs.writeFile('hsm2018.profile.csv', profileCsv, function(err) {
                if(err) {
                    winston.error(err);
                    return;
                }
                winston.info(result.length);
            });
            mongoose.connection.close();
        }, (err) => {
            winston.error('Erro na busca' + err);
        });
};
