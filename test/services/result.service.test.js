const chai = require('chai');
const sinon = require('sinon');
const should = chai.should();
const ResultService = require('../../services/result.service');

const fixture = [
    { answers: [{ value: 'TRANSFORMACAO_DIGITAL' },
                { value: 'TRANSFORMACAO_DIGITAL' },
                { value: 'TRANSFORMACAO_DIGITAL' },
                { value: 'OTHER' },
                { value: 'TRANSFORMACAO_DIGITAL' }] },

    { answers: [{ value: 'INOVACAO_PRODUTOS' },
                { value: 'INOVACAO_PRODUTOS' },
                { value: 'INOVACAO_PRODUTOS' },
                { value: 'OTHER' },
                { value: 'TRANSFORMACAO_DIGITAL' }] },

    { answers: [{ value: 'LIDERANCA_ADAPTATIVA' },
                { value: 'INOVACAO_PRODUTOS' },
                { value: 'LIDERANCA_ADAPTATIVA' },
                { value: 'OTHER' },
                { value: 'TRANSFORMACAO_DIGITAL' }] }
];

const expected = [
    'TRANSFORMACAO_DIGITAL',
    'INOVACAO_PRODUTOS',
    'LIDERANCA_ADAPTATIVA'
];

describe('Result Service', () => {
    describe('Calculate TRANSFORMACAO_DIGITAL profile', () => {
        it('should return Transformação Digital', (done) => {
            const service = new ResultService(fixture[0]);
            service.calculateProfile().should.equal(expected[0]);
            done();
        });
    });

    describe('Calculate INOVACAO_PRODUTOS profile', () => {
        it('should return Inovação de Produtos', (done) => {
            const service = new ResultService(fixture[1]);
            service.calculateProfile().should.equal(expected[1]);
            done();
        });
    });

    describe('Calculate LIDERANCA_ADAPTATIVA profile', () => {
        it('should return Liderança Adaptativa', (done) => {
            const service = new ResultService(fixture[2]);
            service.calculateProfile().should.equal(expected[2]);
            done();
        });
    });
});
