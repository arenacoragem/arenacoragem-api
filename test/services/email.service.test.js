require('dotenv').load();
const chai = require('chai');
const should = chai.should();
const EmailService = require('../../services/email.service');

let fixture = {
    email: 'thersan@thoughtworks.com',
    name: 'Thiago Hersan',
    image_url: 'https://s3.amazonaws.com/tw-arena-coragem-2018/I494CA_JNL23RCP.jpg',
    article_image_url: 'https://s3.amazonaws.com/tw-coragem/INOVACAO_PRODUTOS.jpg',
    article_url: 'https://medium.com/coragem/tudo-pronto-e-funcionando-em-3-meses-c855dcb48061',
    article_title: 'Tudo pronto e funcionando em 3 meses',
    article_intro: 'Apple, Facebook, Spotify e AirBnB. O que essas empresas teriam em comum? Suas idealizadoras trabalharam de forma simples e efetiva, colocando produtos no mercado com rapidez e evoluindo com base na necessidade real das pessoas que utilizam seus produtos e serviços. Atitudes que minimizam seus gastos e fazem você existir no agora. Pois em tempos de inovação disruptiva  —  aquela que altera o seu negócio pela raiz  —  faz-se urgente saber lidar com a aceleração digital. Afinal, você não quer desperdiçar tempo, dinheiro e energia construindo um produto que não vai atender às expectativas de seu público final, certo?'
};

describe('Email Service', () => {
    describe('Mustaching', () => {
        let html;
        let text;
        let filterImages = (k) => {
            return ((k !== 'image_url') &&
                    (k !== 'article_image_url'));
        }
        let filterProfile = (k) => {
            return (k !== 'email');
        }

        before(() => {
            html = EmailService.resultHtml(fixture);
            text = EmailService.resultText(fixture);
        });

        it('plaintext should have rendered all fields (except email and images)', (done) => {
            Object.keys(fixture)
              .filter(filterProfile)
              .filter(filterImages)
              .forEach((k) => {
                text.replace(/\n/g, ' ').should.include.string(fixture[k]);
            });
            done();
        });

        it('html should have rendered all fields (except email)', (done) => {
            Object.keys(fixture)
              .filter(filterProfile)
              .forEach((k) => {
                html.should.include.string(fixture[k]);
            });
            done();
        });

        it('plaintext should not have html tags', (done) => {
            should.equal(EmailService.resultText(fixture).match(/<[^>]*>/g), null);
            done();
        });
    });

    describe('sendResultEmail()', () => {
        it('returns a Promise', (done) => {
            EmailService.sendResultEmail(fixture).should.be.a('Promise');
            done();
        });
    });
});
