require('dotenv').load();

const mongoose = require('mongoose');
const winston = require('winston');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const person = require('./routes/person');
const question = require('./routes/question');
const profile = require('./routes/profile');

const PORT = process.env.PORT || 5050;
const app = express();

mongoose.connect(process.env.MONGODB_URI).then(() => { 
    winston.log('info', 'Connected to database...');
});

const corsOptions = {
    origin: [ process.env.CORS_ORIGIN_GH, process.env.CORS_ORIGIN_GL ],
    optionsSuccessStatus: 200
};

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));
app.use(bodyParser.json({ limit: '5mb' }));

app.get('/', (req, res) => {
    res.status(200).json({ message: 'API OK' });
});

person(app);
question(app);
profile(app);

let server = app.listen(PORT, () => {
    winston.log('info', `Listening @ ${PORT}`);
});

module.exports = server;
